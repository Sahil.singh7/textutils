from django import forms
from django.forms import ModelForm
from .models import *

class Form(forms.ModelForm):
    class Meta:
        model=UserForm
        fields='__all__'
        widgets={
            'name': forms.TextInput(attrs={'class':'form-control'}),
            'roll': forms.NumberInput(attrs={'class':'form-control'}),
            'email': forms.EmailInput(attrs={'class':'form-control'})
        }
