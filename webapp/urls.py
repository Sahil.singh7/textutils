from django.contrib import admin
from django.urls import path
from webapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.signupPage,name='signup'),
    path('login',views.loginPage,name='login'),
    path('homepage',views.homePage,name='homepage'),
    path('logout',views.logoutPage,name='logout'),
    path('result',views.textUtils,name='result'),
    path('form',views.formPage,name='form')
]