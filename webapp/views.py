from django.shortcuts import render,HttpResponse,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from .models import *
from .forms import *

# Create your views here.

def signupPage(request):
    if request.method=='POST':
        uname=request.POST.get('username')
        email=request.POST.get('email')
        pass1=request.POST.get('password1')
        pass2=request.POST.get('password2')
        if pass1!=pass2:
            return HttpResponse('Password does not match')
        else:
            my_user=User.objects.create_user(uname,email,pass1)
            my_user.save()
            return redirect('login')
    return render(request,'signup.html')


def loginPage(request):
    if request.method=='POST':
        username=request.POST.get('username')
        password=request.POST.get('password')
        user=authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect('homepage')
        else:
            return HttpResponse('<h1>Invalid Username or Password</h1>')
    return render(request,'login.html')


def logoutPage(request):
    logout(request)
    return redirect('login')


login_required(login_url='login')
def homePage(request):
    return render(request,'homepage.html')

login_required(login_url='login')
def textUtils(request):
    text=request.POST.get('textarea','default')
    checkbox=request.POST.get('checkbox','off')
    toupper=request.POST.get('uppercase','off')

    if checkbox=='on' and toupper=='on':
        punctuations='''!()-[]={;}:'"\,<>./?@#$%^&*_~'''
        analyseText=''
        for char in text:
            if char not in punctuations:
                analyseText=analyseText+char
                analyseText=analyseText.upper()
        param={
            'output':analyseText,
            'prev':text
        } 
    
    elif toupper=='on':
        analyseText=''
        for char in text:
            analyseText= analyseText + char.upper()
        param={
            'output':analyseText,
            'prev':text
        }
    elif checkbox=='on':
        punctuations='''!()-[]={;}:'"\,<>./?@#$%^&*_~'''
        analyseText=''
        for char in text:
            if char not in punctuations:
                analyseText=analyseText+char
        param={
            'output':analyseText,
            'prev':text
        }
    else:
        return HttpResponse('<h1>Tick the Checkbox</h1>')
        
    return render(request,'result.html',param)

def formPage(request):
    user_form=Form()
    context={
        'form':user_form
    }
    if request.method=='POST':
        user_form=Form(request.POST)
        if user_form.is_valid():
            user_form.save()
    return render(request,'form.html',context)



